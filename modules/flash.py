import tornado.web


class FlashModule(tornado.web.UIModule):
    def render(self):
        return self.render_string("modules/flash.html")