import os
import shutil
import datetime

import tornado.web

from bson import ObjectId
from routes.base import BaseHandler


class PropertyHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        properties = self.db.properties.find({"user_id": self.current_user["_id"]})
        self.render("pages/properties/index.html", properties=properties)


class PropertyView(BaseHandler):
    @tornado.web.authenticated
    def get(self, pid):
        a_property = self.db.properties.find_one({"user_id": self.current_user["_id"], "_id": ObjectId(pid)})
        units = self.db.units.find({"user_id": self.current_user["_id"], "property_id": a_property["_id"] })
        if a_property:
            self.render("pages/properties/view.html", a_property=a_property, units=units)
        else:
            self.set_flash("error", "That property doesn't exist")
            self.redirect("/properties")


class PropertyNew(BaseHandler):
    @tornado.web.authenticated
    def get(self, pid=None):
        a_property = None
        if pid:
            a_property = self.db.properties.find_one({"user_id": self.current_user["_id"], "_id": ObjectId(pid)})
        self.render("pages/properties/new.html", a_property=a_property)

    @tornado.web.authenticated
    def post(self, pid=None):
        a_property = {
            'user_id': self.current_user["_id"],
            "name": None,
            "description": "",
            "photos": [],
            "contact_info": {},
            "location": {},
            "amenities": [],
            "credential": {},
            "targets": [],
            "created_on": datetime.datetime.now(),
            "updated_on": datetime.datetime.now()
        }
        if pid:
            a_property = self.db.properties.find_one({"user_id": self.current_user["_id"], "_id": ObjectId(pid)})
        a_property["name"] = self.get_argument("name")
        keys = ["street_number", "street_name", "route", "full_address", "neighborhood", "city", "state", "zip", "lat", "long"]
        for k in keys:
            a_property["location"][k] = self.get_argument("address_" + k)
        if pid:
            self.db.properties.save(a_property)
        else:
            pid = self.db.properties.insert(a_property)
        self.redirect("/properties/" + str(pid) + "/description")


class PropertyDescription(BaseHandler):
    @tornado.web.authenticated
    def get(self, pid):
        a_property = self.db.properties.find_one({"user_id": self.current_user["_id"], "_id": ObjectId(pid)})
        if a_property:
            self.render("pages/properties/description.html", a_property=a_property)
        else:
            self.set_flash("error", "That property doesn't exist")
            self.redirect("/properties")

    @tornado.web.authenticated
    def post(self, pid):
        a_property = self.db.properties.find_one({"user_id": self.current_user["_id"], "_id": ObjectId(pid)})
        if not a_property:
            self.set_flash("error", "Property not found!")
            self.redirect("/properties")
            return
        a_property["description"] = self.get_argument("description")
        a_property["amenities"] = self.get_arguments("amenity")
        self.db.properties.save(a_property)
        self.redirect("/properties/" + str(pid) + "/photos")


class PropertyPhotos(BaseHandler):
    @tornado.web.authenticated
    def get(self, pid):
        a_property = self.db.properties.find_one({"user_id": self.current_user["_id"], "_id": ObjectId(pid)})
        if a_property:
            self.render("pages/properties/photo.html", a_property=a_property)
        else:
            self.set_flash("error", "That property doesn't exist")
            self.redirect("/properties")

    @tornado.web.authenticated
    def post(self, pid):
        self.redirect("/properties/" + str(pid) + "/contact_info")


class PropertyContacts(BaseHandler):
    @tornado.web.authenticated
    def get(self, pid):
        a_property = self.db.properties.find_one({"user_id": self.current_user["_id"], "_id": ObjectId(pid)})
        if a_property:
            self.render("pages/properties/contact.html", a_property=a_property)
        else:
            self.set_flash("error", "That property doesn't exist")
            self.redirect("/properties")

    @tornado.web.authenticated
    def post(self, pid):
        a_property = self.db.properties.find_one({"user_id": self.current_user["_id"], "_id": ObjectId(pid)})
        if not a_property:
            self.set_flash("error", "That property doesn't exist")
            self.redirect("/properties")
            return
        keys = ["person", "phone_number", "website", "email"]
        for k in keys:
            a_property["contact_info"][k] = self.get_argument("contact_"+k)
        self.db.properties.save(a_property)
        self.redirect("/properties/" + str(pid) + "/preferences")


class PropertyPreferences(BaseHandler):
    @tornado.web.authenticated
    def get(self, pid):
        a_property = self.db.properties.find_one({"user_id": self.current_user["_id"], "_id": ObjectId(pid)})
        credentials = self.db.credentials.find({"user_id": self.current_user["_id"]})
        if a_property:
            self.render("pages/properties/preferences.html", a_property=a_property, credentials=credentials)
        else:
            self.set_flash("error", "That property doesn't exist")
            self.redirect("/properties")

    @tornado.web.authenticated
    def post(self, pid):
        a_property = self.db.properties.find_one({"user_id": self.current_user["_id"], "_id": ObjectId(pid)})
        if not a_property:
            self.set_flash("error", "That property doesn't exist")
            self.redirect("/properties")
            return
        credential = self.db.credentials.find_one({"_id": ObjectId(self.get_argument("credential"))})
        if not credential:
            self.set_flash("error", "That credential doesn't exist")
            self.redirect("/properties/" + str(pid) + "/preferences")
            return
        a_property["credential"] = credential
        a_property["targets"] = self.get_arguments("website")
        self.db.properties.save(a_property)
        self.redirect("/properties/" + str(pid) + "/units")


class PropertyPush(BaseHandler):
    @tornado.web.authenticated
    def get(self, pid):
        a_property = self.db.properties.find_one({"user_id": self.current_user["_id"], "_id": ObjectId(pid)})
        units = self.db.units.find({"property_id": ObjectId(pid)})
        if a_property:
            self.render("pages/properties/push.html", a_property=a_property, units=units)
        else:
            self.set_flash("error", "That property doesn't exist")
            self.redirect("/properties")

    @tornado.web.authenticated
    def post(self, pid):
        a_property = self.db.properties.find_one({"user_id": self.current_user["_id"], "_id": ObjectId(pid)})
        units = self.db.units.find({"property_id": ObjectId(pid)})
        credential = self.db.credentials.find_one({"_id": a_property["credential"]["_id"]}) if a_property["credential"] else None

        if not a_property:
            self.set_flash("error", "That property doesn't exist!")
            self.redirect("/properties")
            return
        if units.count() == 0:
            self.set_flash("error", "This property has no units to push!")
            self.redirect("/properties/" + str(pid) + "/push")
            return
        if len(a_property['targets']) == 0:
            self.set_flash("error", "This property has no target websites!")
            self.redirect("/properties/" + str(pid) + "/push")
            return
        if not a_property['location']['full_address']:
            self.set_flash("error", "This property has no full address on record!")
            self.redirect("/properties/" + str(pid) + "/push")
            return
        if not a_property['description']:
            self.set_flash("error", "This property has no description!")
            self.redirect("/properties/" + str(pid) + "/push")
            return
        if not a_property['contact_info'].get('phone_number', ''):
            self.set_flash("error", "This property has no phone number!")
            self.redirect("/properties/" + str(pid) + "/push")
            return
        if not a_property['contact_info'].get('email', ''):
            self.set_flash("error", "This property has no email address!")
            self.redirect("/properties/" + str(pid) + "/push")
            return
        if not credential:
            self.set_flash("error", "The credential associated with this property doesn't exist!")
            self.redirect("/properties/" + str(pid) + "/push")
            return

        a_property["property_id"] = a_property["_id"]
        del a_property["_id"]
        del a_property["created_on"]
        del a_property["updated_on"]
        a_property["units"] = [unit for unit in units]
        a_property["created_on"] = datetime.datetime.now()
        a_property["updated_on"] = datetime.datetime.now()
        a_property["status"] = "queued"

        self.db.jobs.insert(a_property)
        self.set_flash("success", "Your push has been scheduled and links will appear here soon!")
        self.redirect("/properties/" + str(pid) + "/trackers")


class PropertyDelete(BaseHandler):
    @tornado.web.authenticated
    def get(self, pid):
        a_property = self.db.properties.find_one({"user_id": self.current_user["_id"], "_id": ObjectId(pid)})
        if a_property:
            self.db.units.remove({"property_id": a_property["_id"]})
            self.db.properties.remove({"_id": a_property["_id"]})
            if os.path.exists(os.path.join(self.application.settings['static_path'], "photos", pid)):
                shutil.rmtree(os.path.join(self.application.settings['static_path'], "photos", pid))
            self.set_flash("info", "That property has been deleted")
            self.redirect("/properties")
            return
        else:
            self.set_flash("error", "That property doesn't exist")
            self.redirect("/properties")