import os
import tornado.web
from tornado.escape import json_encode

from hashlib import md5
from PIL import Image
from io import BytesIO
from bson import ObjectId

from routes.base import BaseHandler


class PhotoList(BaseHandler):
    @tornado.web.authenticated
    def get(self, pid):
        a_property = self.db.properties.find_one({"_id": ObjectId(pid)})
        if a_property:
            self.set_status(200)
            self.finish(json_encode([{"name": p} for p in a_property['photos']]))
        else:
            self.set_status(404)
            self.finish()


class PhotoUpload(BaseHandler):
    @tornado.web.authenticated
    def post(self, pid):
        target = os.path.join(os.path.join(self.application.settings["static_path"], "photos", str(pid)))
        if not os.path.exists(target):
            os.makedirs(target)
        a_property = self.db.properties.find_one({"_id": ObjectId(pid)})
        if not a_property:
            self.set_status(500, "Property not found")
            self.finish()
            return
        photo = self.request.files['photo'][0]['body']
        name = md5(self.request.files['photo'][0]['filename'].encode("utf-8")).hexdigest()
        img = Image.open(BytesIO(photo))
        img.save(os.path.join(target, name+ "." + str(img.format).lower()), img.format)
        a_property["photos"].append(name+ "." + str(img.format).lower())
        self.db.properties.save(a_property)
        self.set_status(200)
        self.finish(name)


class PhotoDelete(BaseHandler):
    @tornado.web.authenticated
    def get(self, pid):
        name = self.get_argument("name")
        location = os.path.join(self.application.settings["static_path"], "photos", str(pid), name)
        a_property = self.db.properties.find_one({"_id": ObjectId(pid)})
        if name in a_property["photos"]:
            a_property["photos"].remove(name)
            os.remove(location)
            self.db.properties.save(a_property)
            self.set_status(200, "Removed photo")
            self.finish()
            return
        else:
            self.set_status(404, "Not found")
            self.finish()