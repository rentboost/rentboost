import tornado.web

from bson import ObjectId
from routes.base import BaseHandler


class UnitHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self, pid):
        a_property = self.db.properties.find_one({"user_id": self.current_user["_id"], "_id": ObjectId(pid)})
        if a_property:
            units = self.db.units.find({"property_id": ObjectId(pid)})
            self.render("pages/units/index.html", a_property=a_property, units=units)
        else:
            self.set_flash("error", "That property doesn't exist")
            self.redirect("/properties")


class UnitEdit(BaseHandler):
    @tornado.web.authenticated
    def get(self, pid, uid=None):
        a_property = self.db.properties.find_one({"_id": ObjectId(pid)})
        if not a_property:
            self.set_flash("error", "That property doesn't exist")
            self.redirect("/properties")
            return
        a_unit = {}
        if uid:
            a_unit = self.db.units.find_one({"_id": ObjectId(uid)})
        info = {"location": a_property["location"]["neighborhood"], "amenities": a_property["amenities"]}
        self.render("pages/units/new.html", a_property=a_property, a_unit=a_unit, info=info)

    def post(self, pid, uid=None):
        a_property = self.db.properties.find_one({"_id": ObjectId(pid)})
        if not a_property:
            self.set_flash("error", "That property doesn't exist")
            self.redirect("/properties")
            return
        a_unit = {'user_id': self.current_user["_id"], 'property_id': a_property["_id"]}
        if uid:
            a_unit = self.db.units.find_one({"_id": ObjectId(uid)})
        keys = ["unit_number", "unit_available_date", "unit_lease_duration", "unit_bedrooms", "unit_bathrooms",
                "unit_sqft", "unit_fee", "unit_title"]
        for key in keys:
            a_unit[key] = self.get_argument(key)
        float_keys = ["unit_rent", "unit_deposit"]
        for key in float_keys:
            a_unit[key] = int(self.get_argument(key))
        a_unit["amenities"] = self.get_arguments("amenity")
        if uid:
            self.db.units.save(a_unit)
        else:
            self.db.units.insert(a_unit)
        self.redirect("/properties/" + str(pid) + "/units")


class UnitDelete(BaseHandler):
    @tornado.web.authenticated
    def get(self, pid, uid):
        a_unit = self.db.units.find_one({"_id": ObjectId(uid), "property_id": ObjectId(pid)})
        if not a_unit:
            self.set_flash("error", "That unit doesn't exist")
            self.redirect("/properties/" + str(pid) + "/units")
            return
        self.db.units.remove(a_unit)
        self.set_flash("info", "That unit has been deleted")
        self.redirect("/properties/" + str(pid) + "/units")


class TrackerHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self, pid):
        a_property = self.db.properties.find_one({"user_id": self.current_user["_id"], "_id": ObjectId(pid)})
        if a_property:
            trackers = self.db.trackers.find({"property_id": a_property["_id"]})
            self.render("pages/units/trackers.html", a_property=a_property, trackers=trackers)
        else:
            self.set_flash("error", "That property doesn't exist")
            self.redirect("/properties")