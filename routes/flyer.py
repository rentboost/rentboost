import tornado.web

from bson import ObjectId
from routes.base import BaseHandler


class FlyerHandler(BaseHandler):
    def get(self, pid):
        a_property = self.db.properties.find_one({"_id": ObjectId(pid)})
        units = self.db.units.find({"property_id": a_property["_id"] })
        self.render("pages/flyers/index.html", a_property=a_property, units=units)