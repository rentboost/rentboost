import datetime

import tornado.web

from bson import ObjectId
from routes.base import BaseHandler


class UserBillingHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        self.render("pages/user/billing.html")


class UserProfileHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        self.render("pages/user/profile.html")

    @tornado.web.authenticated
    def post(self):
        changing_password = False

        if self.empty_field(self.get_argument("name")):
            self.set_flash("warning", "I wanna know your name ;)")
            self.redirect("/user/profile")
            return

        if not self.valid_email(self.get_argument("email")):
            self.set_flash("error", "Please enter a valid email")
            self.redirect("/user/profile")
            return

        if not self.empty_field(self.get_argument("current_password")) and not self.empty_field(
                self.get_argument("password")):
            changing_password = True

            hashed_password = self.get_password(self.get_argument("current_password"))
            if hashed_password != self.current_user["profile"]["password"]:
                self.set_flash("error", "You entered a wrong current password")
                self.redirect("/user/profile")
                return

            if not self.valid_password(self.get_argument("password")):
                self.set_flash("error", "Password must be at least 6 characters long")
                self.redirect("/user/profile")
                return

            if not self.confirm_password(self.get_argument("password"), self.get_argument("password_confirmation")):
                self.set_flash("error", "Password and confirmation do not match")
                self.redirect("/user/profile")
                return

        user = self.db.users.find_one({"_id": ObjectId(self.current_user["_id"])})
        form_fields = ["name", "email"]
        for field in form_fields:
            user["profile"][field] = self.get_argument(field)
        if changing_password:
            hashed_password = self.get_password(self.get_argument("password"))
            user["profile"]["password"] = hashed_password
        user["profile"]["updated_on"] = datetime.datetime.now()
        self.db.users.save(user)
        self.set_flash("success", "Your settings have been updated")
        self.redirect("/user/profile")


class UserSettingsHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        self.render("pages/user/settings.html")