import tornado.web

from bson import ObjectId, SON
from routes.base import BaseHandler


class CredentialHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self, kid=None):
        credentials = self.db.credentials.find({"user_id": self.current_user["_id"]})
        self.render("pages/credentials/index.html", credentials=credentials)


class CredentialEdit(BaseHandler):
    @tornado.web.authenticated
    def get(self, kid=None):
        credential = None
        if kid:
            credential = self.db.credentials.find_one({"_id": ObjectId(kid), "user_id": self.current_user["_id"]})
        self.render("pages/credentials/new.html", credential=credential)

    def post(self, kid=None):
        credential = {}
        if kid:
            credential = self.db.credentials.find_one({"_id": ObjectId(kid), "user_id": self.current_user["_id"]})
        credential["keys"] = SON(
            zip(["postlets", "padlister", "vlshomes", "house", "ebay", "rentdigs"],
                zip(self.get_arguments("website_username"), self.get_arguments("website_password"))))
        credential["name"] = self.get_argument("name")
        credential["user_id"] = self.current_user["_id"]
        if kid:
            self.db.credentials.save(credential)
        else:
            self.db.credentials.insert(credential)
        self.redirect("/credentials")


class CredentialDelete(BaseHandler):
    @tornado.web.authenticated
    def get(self, kid):
        kid = self.db.credentials.find_one({"_id": ObjectId(kid), "user_id": self.current_user["_id"]})
        if not kid:
            self.set_flash("error", "That credential doesn't exist")
            self.redirect("/credentials")
            return
        properties = self.db.properties.find({"details.credential._id": kid["_id"]})
        for p in properties:
            p["credential"] = {}
            self.db.properties.save(p)
        self.db.credentials.remove({"_id": kid["_id"]})
        self.set_flash("info", "That credential has been deleted")
        self.redirect("/credentials")