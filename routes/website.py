from routes.base import BaseHandler


class IndexHandler(BaseHandler):
    def get(self):
        # self.render("website/index.html")
        self.redirect("/dashboard")