import re
import datetime


class Validator(object):
    def empty_field(self, field):
        if field in ("", None):
            return True
        return False

    def valid_email(self, email):
        if re.match("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$", email):
            return True
        return False

    def valid_password(self, password):
        return len(password) >= 6

    def confirm_password(self, password, confirmation):
        return password == confirmation

    def expired_reset_link(self, date):
        return date > (date + datetime.timedelta(hours=24))