import urllib.parse
import tornado.httpclient


class Mailer(object):
    def __init__(self, api_key):
        self.API_KEY = api_key
        self.BASE_URL = "https://api.mailgun.net/v2/mailer.rentboost.com"
        self.client = tornado.httpclient.HTTPClient()

    def send(self, data):
        data = urllib.parse.urlencode(data)
        request = tornado.httpclient.HTTPRequest(self.BASE_URL + "/messages", "POST", auth_username="api",
                                                 auth_password=self.API_KEY, body=data)
        response = self.client.fetch(request)
        if response.code == 200:
            return True
        return False

    def new_user(self, email):
        data = {"from": "RentBoost <no-reply@mailer.rentboost.com>", "to": email, "subject": "Welcome to RentBoost!",
                "text": "Thanks for signing up!"}
        if self.send(data):
            return True
        return False

    def reset_password(self, email, code):
        data = {"from": "RentBoost <no-reply@mailer.rentboost.com>", "to": email, "subject": "Password reset",
                "text": "To reset your password, please visit this link http://www.rentboost.com/user/forgot/" + code}
        if self.send(data):
            return True
        return False
        
    def waitlist(self, email):
        data = {"from": "RentBoost <no-reply@mailer.rentboost.com>", "to": email, "subject": "Thanks for your interest!",
                "text": "Hello,\n\nThanks for your interest! You will be among first users to have access to our platform when we launch. Stay tuned!\n\nRentBoost"}
        if self.send(data):
            return True
        return False