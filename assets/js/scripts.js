$(function() {
    $('#property-delete').click(function() {
       return confirm("Deleting a property will also delete its units, trackers and photos. This action can't be undone. Are you sure?");
    });
    $('#unit-delete').click(function() {
        return confirm("Deleting a unit will delete it from external websites. This action can't be undone. Are you sure?");
    });
    $('#new-property-location').bootstrapValidator({
        fields: {
            raw_address: {
                validators: {
                    notEmpty: {
                        message: 'Please enter an accurate address'
                    }
                }
            },
            address_route: {
                validators: {
                    notEmpty: {
                        message: 'The street address is required'
                    }
                }
            },
            address_neighborhood: {
                validators: {
                    notEmpty: {
                        message: 'The neighborhood is required'
                    }
                }
            },
            address_city: {
                validators: {
                    notEmpty: {
                        message: 'The city is required'
                    }
                }
            },
            address_state: {
                validators: {
                    notEmpty: {
                        message: 'The state is required'
                    }
                }
            },
            address_zip: {
                validators: {
                    notEmpty: {
                        message: 'The ZIP code is required'
                    },
                    zipCode: {
                        country: 'US',
                        message: 'The ZIP code is invalid'
                    }
                }
            }
        }
    });
    $('#new-property-description').bootstrapValidator({
        fields: {
            description: {
                validators: {
                    notEmpty: {
                        message: 'The description is required'
                    }
                }
            }
        }
    });
    $('#new-property-contacts').bootstrapValidator({
        fields: {
            contact_person: {
                validators: {
                    notEmpty: {
                        message: 'A contact person is required'
                    }
                }
            },
            contact_phone_number: {
                validators: {
                    notEmpty: {
                        message: 'The phone number is required'
                    }
                }
            },
            contact_website: {
                validators: {
                    uri: {
                        message: 'The website must be valid e.g. http://www.property.com'
                    }
                }
            },
            contact_email: {
                validators: {
                    validators: {
                        notEmpty: {
                            message: 'The email address is required'
                        }
                    },
                    emailAddress: {
                        message: 'The email must be valid'
                    }
                }
            }
        }
    });
    $('#new-unit').bootstrapValidator({
        fields: {
            unit_rent: {
                validators: {
                    notEmpty: {
                        message: 'The rent is required'
                    },
                    numeric: {
                        message: 'The rent must be a number'
                    }
                }
            },
            unit_available_date: {
                validators: {
                    date: {
                        format: 'MM/DD/YYYY',
                        message: 'This is not a valid date'
                    }
                }
            }
        }
    });
    $('#signin').bootstrapValidator({
        fields: {
            email: {
                validators: {
                    notEmpty: {
                        message: 'Email is required'
                    },
                    emailAddress: {
                        message: "Valid email is required"
                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: 'Password is requried'
                    },
                    stringLength: {
                        min: 6,
                        message: 'Password must be at least 6 character long'
                    }
                }
            }
        }
    });
    $('#signup').bootstrapValidator({
        fields: {
            name: {
                validators: {
                    notEmpty: {
                        message: 'Name is required'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'Email is required'
                    },
                    emailAddress: {
                        message: "Valid email is required"
                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: 'Password is requried'
                    },
                    stringLength: {
                        min: 6,
                        message: 'Password must be at least 6 characters long'
                    }
                }
            },
            password_confirmation: {
                validators: {
                    notEmpty: {
                        message: 'Password confirmation is requried'
                    },
                    identical: {
                        field: 'password',
                        message: 'Password and confirmation must match'
                    }
                }
            }
        }
    });
    $('#forgot').bootstrapValidator({
        fields: {
            email: {
                validators: {
                    notEmpty: {
                        message: 'Email is required'
                    },
                    emailAddress: {
                        message: "Valid email is required"
                    }
                }
            }
        }
    });
    $('#forgot-reset').bootstrapValidator({
        fields: {
            password: {
                validators: {
                    stringLength: {
                        min: 6,
                        message: 'Password must be at least 6 characters long'
                    }
                }
            },
            password_confirmation: {
                validators: {
                    identical: {
                        field: 'password',
                        message: 'Password and confirmation must match'
                    }
                }
            }
        }
    });
    $('#profile-form').bootstrapValidator({
        fields: {
            name: {
                validators: {
                    notEmpty: {
                        message: 'Name is required'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'Email is required'
                    },
                    emailAddress: {
                        message: "Valid email is required"
                    }
                }
            },
            current_password: {
                validators: {
                    stringLength: {
                        min: 6,
                        message: 'Your current password is probably at least 6 characters long'
                    }
                }
            },
            password: {
                validators: {
                    stringLength: {
                        min: 6,
                        message: 'Password must be at least 6 characters long'
                    }
                }
            },
            password_confirmation: {
                validators: {
                    identical: {
                        field: 'password',
                        message: 'Password and confirmation must match'
                    }
                }
            }
        }
    });
});