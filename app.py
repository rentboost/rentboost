# !/usr/bin/env python3

import os

import tornado.ioloop
import tornado.httpserver
import tornado.web

import routes.website
import routes.auth
import routes.user
import routes.base
import routes.property
import routes.flyer
import routes.units
import routes.credential
import routes.photo

from modules.flash import FlashModule

from utils.mailer import Mailer
from pymongo import MongoClient
from tornado.options import define, options, parse_command_line

define("port", default=8080, type=int)


class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r"/", routes.website.IndexHandler),
            (r"/user/signin/?", routes.auth.SigninHandler),
            (r"/user/signup/?", routes.auth.SignupHandler),
            (r"/user/signout/?", routes.auth.SignoutHandler),
            (r"/user/forgot/?", routes.auth.UserForgotHandler),
            (r"/user/forgot/([0-9a-f]{32})/?", routes.auth.UserForgotResetHandler),
            (r"/user/settings/?", routes.user.UserSettingsHandler),
            (r"/user/profile/?", routes.user.UserProfileHandler),
            (r"/user/billing/?", routes.user.UserBillingHandler),
            (r"/dashboard/?", routes.base.DashboardHandler),
            (r"/properties/?", routes.property.PropertyHandler),
            (r"/properties/new", routes.property.PropertyNew),
            (r"/properties/([0-9a-f]{24})/view", routes.property.PropertyView),
            (r"/properties/([0-9a-f]{24})/location", routes.property.PropertyNew),
            (r"/properties/([0-9a-f]{24})/description", routes.property.PropertyDescription),
            (r"/properties/([0-9a-f]{24})/units", routes.units.UnitHandler),
            (r"/properties/([0-9a-f]{24})/units/new", routes.units.UnitEdit),
            (r"/properties/([0-9a-f]{24})/units/([0-9a-f]{24})/edit", routes.units.UnitEdit),
            (r"/properties/([0-9a-f]{24})/units/([0-9a-f]{24})/delete", routes.units.UnitDelete),
            (r"/properties/([0-9a-f]{24})/trackers", routes.units.TrackerHandler),
            (r"/properties/([0-9a-f]{24})/photos", routes.property.PropertyPhotos),
            (r"/properties/([0-9a-f]{24})/contact_info", routes.property.PropertyContacts),
            (r"/properties/([0-9a-f]{24})/preferences", routes.property.PropertyPreferences),
            (r"/properties/([0-9a-f]{24})/push", routes.property.PropertyPush),
            (r"/properties/([0-9a-f]{24})/delete", routes.property.PropertyDelete),
            (r"/flyers/([0-9a-f]{24})/?", routes.flyer.FlyerHandler),
            (r"/credentials/?", routes.credential.CredentialHandler),
            (r"/credentials/add", routes.credential.CredentialEdit),
            (r"/credentials/([0-9a-f]{24})/edit", routes.credential.CredentialEdit),
            (r"/credentials/([0-9a-f]{24})/delete", routes.credential.CredentialDelete),
            (r"/photos/([0-9a-f]{24})", routes.photo.PhotoList),
            (r"/photos/([0-9a-f]{24})/upload", routes.photo.PhotoUpload),
            (r"/photos/([0-9a-f]{24})/delete", routes.photo.PhotoDelete)
        ]
        settings = dict(
            app_name="RentBoost",
            template_path=os.path.join(os.path.dirname(__file__), "views"),
            static_path=os.path.join(os.path.dirname(__file__), "assets"),
            ui_modules=dict(FlashModule=FlashModule),
            mailgun_key="key-5da0f1ae9d0179f64b2bb652f0314f2b",
            password_secret="TODO: SECRET SALT HERE",
            cookie_secret="TODO: SECRET SALT HERE",
            login_url="/user/signin",
            debug=True
        )
        tornado.web.Application.__init__(self, handlers, **settings)

        if self.settings.get("debug", ''):
            self.dbc = MongoClient("mongodb://rentboost:rentboost@linus.mongohq.com:10056/rentboost")
        else:
            self.dbc = MongoClient()
        self.db = self.dbc.rentboost
        self.mailer = Mailer(self.settings["mailgun_key"])


def main():
    try:
        parse_command_line()
        http_server = tornado.httpserver.HTTPServer(Application())
        http_server.listen(options.port)
        tornado.ioloop.IOLoop.instance().start()
    except KeyboardInterrupt:
        print("\nStopping application...\n")


if __name__ == "__main__":
    main()