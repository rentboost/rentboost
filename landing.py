# !/usr/bin/env python3

import os
import datetime

import tornado.ioloop
import tornado.httpserver
import tornado.web

from utils.mailer import Mailer
from utils.validator import Validator

from pymongo import MongoClient
from tornado.options import define, options, parse_command_line

define("port", default=8000, type=int)


class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r"/", IndexHandler)
        ]
        settings = dict(
            template_path=os.path.join(os.path.dirname(__file__), "views", "pages", "landing"),
            debug="True"
        )
        tornado.web.Application.__init__(self, handlers, **settings)

        if self.settings.get("debug", ''):
            self.dbc = MongoClient("mongodb://rentboost:rentboost@linus.mongohq.com:10056/rentboost")
        else:
            self.dbc = MongoClient()
        self.db = self.dbc.rentboost
        self.mailer = Mailer("key-5da0f1ae9d0179f64b2bb652f0314f2b")


class IndexHandler(tornado.web.RequestHandler, Validator):
    def get(self):
        self.render("index.html")

    def post(self):
        email = self.get_argument("email")
        if not self.valid_email(email):
            self.set_status(400)
            self.finish()
            return

        if self.application.db.waitlist.find({"email": email}).count() == 0:
            self.application.db.waitlist.insert({"email": email, "when": datetime.datetime.now()})
            self.application.mailer.waitlist(email)
            self.set_status(200, "Cool, now on waitlist!")
            self.finish()
            return
        else:
            self.set_status(200, "Already on waitlist!")
            self.finish()


def main():
    try:
        parse_command_line()
        http_server = tornado.httpserver.HTTPServer(Application())
        http_server.listen(options.port)
        tornado.ioloop.IOLoop.instance().start()
    except KeyboardInterrupt:
        print("\nStopping application...\n")


if __name__ == "__main__":
    main()